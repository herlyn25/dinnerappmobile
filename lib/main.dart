import 'package:curso_udemy/category/provider/category_provider.dart';
import 'package:curso_udemy/restaurant/provider/restaurant_provider.dart';
import 'package:curso_udemy/router/app_router.dart';
import 'package:curso_udemy/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const AppState());
}

class AppState extends StatelessWidget {
  const AppState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => RestaurantProvider(),
          lazy: false,
        ),
        ChangeNotifierProvider(
          create: (_) => CategoryProvider(),
          lazy: false,
        )
      ],
      child: const MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: AppRouter.getAppRoutes(),
      theme: AppTheme.lightTheme,
    );
  }
}
