import 'package:flutter/material.dart';

class AppTheme {
  //static const Color primary = Color.fromARGB(255, 38, 193, 101);
  static const Color primary = Colors.red;
  static const Color secondary = Colors.white;
  static const Color tercery = Colors.transparent;
  static final ThemeData lightTheme = ThemeData.light().copyWith(
      primaryColor: primary,
      appBarTheme: const AppBarTheme(color: primary, elevation: 0),
      textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(foregroundColor: secondary)),
      inputDecorationTheme: const InputDecorationTheme(
          hintStyle: TextStyle(color: secondary),
          floatingLabelStyle: TextStyle(color: secondary, fontSize: 28),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: tercery),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  topRight: Radius.circular(10))),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: tercery),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  topRight: Radius.circular(10))),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  topRight: Radius.circular(10)))));

  static final ThemeData darkTheme = ThemeData.dark().copyWith(
      primaryColor: secondary,
      appBarTheme: const AppBarTheme(color: secondary, elevation: 0),
      textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(foregroundColor: secondary)));
}
