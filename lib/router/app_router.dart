import 'package:curso_udemy/export_general.dart';
import 'package:flutter/material.dart';

class AppRouter {
  static final menuOptions = <MenuOptions>[
    MenuOptions(
        name: 'Sign up',
        route: 'signup',
        icon: Icons.sign_language,
        screen: const SignUp()),
    MenuOptions(
        name: "Restauranre 1",
        route: "restaurant1",
        icon: Icons.restaurant,
        screen: const RestaurantHome()),
    MenuOptions(
        name: 'Login',
        route: 'login',
        icon: Icons.login_outlined,
        screen: FormLogin()),
    MenuOptions(
        name: 'Detail Restaurant',
        route: 'detail_restaurant',
        icon: Icons.restaurant,
        screen: const DetailRestaurant())
  ];

  static Map<String, Widget Function(BuildContext context)> getAppRoutes() {
    Map<String, Widget Function(BuildContext)> appRoutes = {};
    //appRoutes.addAll({"/": (context) => const DinnerHome()});
    appRoutes.addAll({"/": (context) => const RestaurantHome()});
    for (final option in menuOptions) {
      appRoutes.addAll({option.route: (BuildContext context) => option.screen});
    }
    return appRoutes;
  }
}
