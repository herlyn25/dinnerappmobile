import 'dart:convert';

import '../../category/models/category.dart';
import '../../restaurant/models/restaurant.dart';

class Product {
  Product({
    this.id,
    required this.name,
    this.amount,
    required this.category,
    required this.restaurant,
    this.photo,
  });

  int? id;
  String name;
  double? amount;
  Category category;
  Restaurant restaurant;
  String? photo;

  factory Product.fromJson(String str) => Product.fromMap(json.decode(str));

  factory Product.fromMap(Map<String, dynamic> json) => Product(
        id: json["id"],
        name: json["name"],
        amount: json["amount"],
        category: Category.fromMap(json["category"]),
        restaurant: Restaurant.fromMap(json["restaurant"]),
        photo: json["photo"],
      );
  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "amount": amount,
        "category": category.toMap(),
        "restaurant": restaurant.toMap(),
        "photo": photo,
      };
}
