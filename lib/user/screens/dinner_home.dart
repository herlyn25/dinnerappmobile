import 'package:curso_udemy/export_general.dart';
import 'package:curso_udemy/theme/app_theme.dart';
import 'package:flutter/material.dart';

class DinnerHome extends StatelessWidget {
  const DinnerHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(50),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.only(top: 30),
                child: Image.asset("assets/logo_mesero.png",
                    height: 300, width: 300),
              ),
              Container(
                margin: const EdgeInsets.only(top: 30),
                child: const Text('Welcome',
                    style: TextStyle(fontFamily: "Poppins", fontSize: 36)),
              ),
              const Text(
                  "Are you hungry? DinnerApp \n helps you to saciate your palate.",
                  style: TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 16,
                      color: AppTheme.primary),
                  textAlign: TextAlign.center),
              ContinueButton(
                texto: "Continue",
                button_color: AppTheme.primary,
                text_color: AppTheme.secondary,
                function_button: () {
                  Navigator.pushNamed(context, "login");
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
