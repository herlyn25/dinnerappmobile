// ignore_for_file: unused_local_variable, non_constant_identifier_names
import 'package:curso_udemy/export_general.dart';
import 'package:curso_udemy/theme/app_theme.dart';
import 'package:flutter/material.dart';

import '../../services/services.dart';

class SignUp extends StatelessWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final globalKey = GlobalKey<FormState>();
    final Map<String, dynamic> valuesForms = {};
    return Scaffold(
      appBar: AppBar(
        title: const Text("Sign Up"),
      ),
      backgroundColor: AppTheme.primary,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
          child: Container(
            margin: const EdgeInsets.only(top: 30),
            child: Form(
              key: globalKey,
              child: Column(
                children: [
                  const Center(child: LogoApp()),
                  TextFormFieldCustom(
                    keyboard: TextInputType.text,
                    icon: Icons.person,
                    hint_text: 'Enter your username',
                    label_text: 'Username',
                    formValues: valuesForms,
                    valueProperty: 'username',
                  ),
                  const Divider(
                    color: AppTheme.secondary,
                  ),
                  TextFormFieldCustom(
                    keyboard: TextInputType.text,
                    icon: Icons.person,
                    hint_text: 'Enter your fullname',
                    label_text: 'Name',
                    formValues: valuesForms,
                    valueProperty: 'first_name',
                  ),
                  const Divider(
                    color: AppTheme.secondary,
                  ),
                  const SizedBox(height: 10),
                  TextFormFieldCustom(
                    keyboard: TextInputType.emailAddress,
                    icon: Icons.email,
                    hint_text: 'Enter your email',
                    label_text: 'Email',
                    formValues: valuesForms,
                    valueProperty: 'email',
                  ),
                  const Divider(
                    color: AppTheme.secondary,
                  ),
                  const SizedBox(height: 10),
                  TextFormFieldCustom(
                    keyboard: TextInputType.number,
                    icon: Icons.phone,
                    hint_text: 'Enter your phone number',
                    label_text: 'Phone Number',
                    formValues: valuesForms,
                    valueProperty: 'phone',
                  ),
                  const Divider(
                    color: AppTheme.secondary,
                  ),
                  const SizedBox(height: 10),
                  TextFormFieldCustom(
                    isPassword: true,
                    keyboard: TextInputType.text,
                    icon: Icons.key,
                    hint_text: 'Enter your password',
                    label_text: 'Password',
                    formValues: valuesForms,
                    valueProperty: 'password',
                  ),
                  const Divider(
                    color: AppTheme.secondary,
                  ),
                  const SizedBox(height: 10),
                  /*DropdownButtonFormField<String>(
                      dropdownColor: Colors.white,
                      value: 'Masculino',
                      items: const [
                        DropdownMenuItem(
                            value: "Masculino",
                            child: Text('Masculino',
                                style: TextStyle(color: Colors.black))),
                        DropdownMenuItem(
                            value: "Femenino",
                            child: Text('Femenino',
                                style: TextStyle(color: Colors.black)))
                      ],
                      onChanged: (value) {
                        valuesForms['sexo'] = value ?? 'Masculino';
                      }),*/
                  ContinueButton(
                    button_color: Colors.black,
                    text_color: AppTheme.secondary,
                    texto: 'Continue',
                    function_button: () async {
                      FocusScope.of(context).requestFocus(FocusNode());
                      if (globalKey.currentState!.validate()) {
                        final status_code = await ServicesApp.registerCustomer(
                            valuesForms, "/customer/");
                        if (status_code == 201) {
                          Navigator.pushNamed(context, "login");
                        } else {
                          const snackBar = SnackBar(
                            content: Text("Can't save data!"),
                          );

                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        }
                      } else {
                        const snackBar = SnackBar(
                          content: Text('Invalid Form!'),
                        );

                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      }
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
