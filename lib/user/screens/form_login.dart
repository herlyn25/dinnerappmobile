// ignore_for_file: must_be_immutable

import 'package:curso_udemy/services/services.dart';
import 'package:curso_udemy/theme/app_theme.dart';
import 'package:flutter/material.dart';

import '../user_export.dart';

class FormLogin extends StatelessWidget {
  FormLogin({Key? key}) : super(key: key);
  Map<String, String> credentials = {};
  @override
  Widget build(BuildContext context) {
    final globalKey = GlobalKey<FormState>();

    TextStyle style = const TextStyle(color: AppTheme.secondary, fontSize: 16);
    return Scaffold(
      backgroundColor: AppTheme.primary,
      body: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        child: Container(
          margin: const EdgeInsets.only(top: 130),
          child: Form(
            key: globalKey,
            child: Column(children: [
              const Center(child: LogoApp()),
              const SizedBox(height: 30),
              TextFormFieldCustom(
                keyboard: TextInputType.text,
                icon: Icons.person,
                hint_text: 'enter your username',
                label_text: 'Username',
                formValues: credentials,
                valueProperty: "username",
              ),
              const Divider(
                color: AppTheme.secondary,
              ),
              const SizedBox(height: 20),
              TextFormFieldCustom(
                isPassword: true,
                keyboard: TextInputType.text,
                icon: Icons.key,
                hint_text: 'enter your password',
                label_text: 'Password',
                formValues: credentials,
                valueProperty: "password",
              ),
              const Divider(
                color: AppTheme.secondary,
              ),
              ContinueButton(
                text_color: AppTheme.secondary,
                button_color: Colors.black,
                texto: "Continue",
                function_button: () async {
                  int status = 0;
                  var snackBar = const SnackBar(
                    content: Text('Failed connection to API!'),
                  );

                  FocusScope.of(context).requestFocus(FocusNode());

                  if (globalKey.currentState!.validate()) {
                    try {
                      status =
                          await ServicesApp.login(credentials, "api/token/");
                      if (status == 200) {
                        Navigator.pushNamed(context, "restaurant1");
                      } else {
                        snackBar = const SnackBar(
                            content: Text("Invalid credentials"));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      }
                    } on Exception {
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    }
                  } else {
                    const snackBar = SnackBar(
                      content: Text('Invalid Form'),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Don't have an account",
                    style: style,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'signup');
                    },
                    child: const Text('Sign up!',
                        style: TextStyle(color: Colors.blue)),
                  )
                ],
              ),
              Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: TextButton(
                    onPressed: () {
                      print("Voy para olvidar contraseña");
                    },
                    child: const Text('Forgot Password?')),
              )
            ]),
          ),
        ),
      )),
    );
  }
}
