// ignore_for_file: non_constant_identifier_names

class User {
  final String full_name;
  final String username;
  final String email;
  final String photo;

  User(
      {required this.full_name,
      required this.username,
      required this.email,
      required this.photo});
}
