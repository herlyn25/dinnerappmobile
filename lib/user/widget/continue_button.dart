// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

class ContinueButton extends StatelessWidget {
  final String texto;
  final Color text_color;
  final Color button_color;
  final Function()? function_button;

  const ContinueButton({
    Key? key,
    required this.button_color,
    required this.text_color,
    required this.texto,
    required this.function_button,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: button_color, borderRadius: BorderRadius.circular(20)),
        margin: const EdgeInsets.only(top: 60),
        width: 270,
        height: 58,
        child: TextButton(
            child: Text(texto, style: TextStyle(color: text_color)),
            onPressed: function_button));
  }
}
