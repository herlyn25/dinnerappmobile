import 'package:flutter/material.dart';
import '../../theme/app_theme.dart';

class LogoApp extends StatelessWidget {
  const LogoApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(30),
      child: Row(children: [
        Stack(children: [
          Container(
              width: 75,
              height: 75,
              decoration: const BoxDecoration(
                  color: AppTheme.secondary,
                  borderRadius: BorderRadius.all(Radius.circular(5)))),
          Transform.rotate(
            angle: -0.4,
            child: Container(
              margin: const EdgeInsets.only(left: 15, top: 10),
              width: 48,
              height: 53,
              decoration: const BoxDecoration(
                  color: AppTheme.primary,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      bottomRight: Radius.circular(30))),
            ),
          ),
          Transform.rotate(
            angle: -0.4,
            child: Container(
              margin: const EdgeInsets.only(left: 20, top: 10),
              child: const Image(
                image: AssetImage('assets/icono_solo.png'),
                fit: BoxFit.cover,
                height: 55,
                width: 30,
              ),
            ),
          )
        ]),
        Container(
          margin: const EdgeInsets.only(left: 5),
          child: const Text('inerApp',
              style: TextStyle(
                  color: AppTheme.secondary,
                  fontSize: 50,
                  fontWeight: FontWeight.bold)),
        )
      ]),
    );
  }
}
