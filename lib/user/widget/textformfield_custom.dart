// ignore_for_file: must_be_immutable, non_constant_identifier_names, avoid_print

import 'package:curso_udemy/theme/app_theme.dart';
import 'package:flutter/material.dart';

class TextFormFieldCustom extends StatelessWidget {
  final IconData icon;
  final String hint_text;
  final String label_text;
  final TextInputType keyboard;
  final bool isPassword;
  String? valueProperty;
  Map<String, dynamic>? formValues;
  TextStyle style = const TextStyle(color: AppTheme.secondary);

  TextFormFieldCustom(
      {Key? key,
      this.isPassword = false,
      required this.icon,
      required this.hint_text,
      required this.label_text,
      required this.keyboard,
      this.valueProperty,
      this.formValues})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: isPassword,
      keyboardType: keyboard,
      autofocus: false,
      initialValue: '',
      textCapitalization: TextCapitalization.words,
      onChanged: (value) => formValues![valueProperty!] = value,
      style: style,
      decoration: InputDecoration(
          hintText: hint_text,
          labelText: label_text,
          labelStyle: style,
          hintStyle: style,
          prefixIcon: Icon(icon, color: AppTheme.secondary)),
      validator: (value) {
        if (value == null) return "this field is required";
        return value.length < 3 ? 'Minium 3 letters' : null;
      },
      autovalidateMode: AutovalidateMode.onUserInteraction,
    );
  }
}
