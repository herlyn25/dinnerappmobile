import 'dart:convert';
import 'package:http/http.dart' as http;

class ServicesApp {
  static const baseUrl = "https://mydinnerapp.herokuapp.com/";
  static const headers = {"Content-Type": "application/json;charset=UTF-8"};

  static login(Map<String, String> credentials, String path) async {
    final url = Uri.parse('$baseUrl$path');
    final response =
        await http.post(url, headers: headers, body: jsonEncode(credentials));
    return response.statusCode;
  }

  static registerCustomer(Map<String, dynamic> data, String path) async {
    final url = Uri.parse('$baseUrl$path');
    final response =
        await http.post(url, headers: headers, body: jsonEncode(data));
    return response.statusCode;
  }
}
