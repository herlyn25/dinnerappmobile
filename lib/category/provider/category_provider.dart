// ignore_for_file: avoid_function_literals_in_foreach_calls
import 'dart:convert';
import 'package:curso_udemy/export_general.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../product/models/product.dart';

class CategoryProvider extends ChangeNotifier {
  String baseUrl = "https://mydinnerapp.herokuapp.com/";
  Map<String, String> headers = {
    "Content-Type": "application/json;charset=UTF-8"
  };
  List<String> categoryRestaurant = [];
  CategoryProvider();

  Future<List<Category>> getCategory(String path, int id) async {
    List<Product> listProducts = [];
    List<Category> category = [];
    List listprueba = [];
    final url = Uri.parse('$baseUrl$path$id');
    final response = await http.get(url, headers: headers);
    try {
      List listProductResponse = json.decode(response.body);
      listProductResponse.forEach((element) {
        listProducts.add(Product.fromMap(element));
      });

      listProducts.forEach((e) => listprueba.add(e.category.toMap()));
      final Map<String, dynamic> mapFilter = {};

      for (Map<String, dynamic> myMap in listprueba) {
        mapFilter[myMap['name']] = myMap;
      }

      final List<Map<String, dynamic>> listFilter = mapFilter.keys
          .map((key) => mapFilter[key] as Map<String, dynamic>)
          .toList();

      listFilter.forEach((element) {
        category.add(Category.fromMap(element));
      });
    } catch (e) {
      print(response.statusCode);
    }
    return category;
  }

  Future<List<Product>> getProductByCategoryAndRestaurant(
      int idRestaurant, int idCategory) async {
    final url = Uri.parse(
        "${baseUrl}product/?restaurant=$idRestaurant&category=$idCategory");
    final response = await http.get(url, headers: headers);
    List<Product> listProducts = [];
    try {
      List listProductResponse = json.decode(response.body);
      listProductResponse.forEach((element) {
        listProducts.add(Product.fromMap(element));
      });
      listProducts.forEach((element) {
        print(element.name);
      });
    } catch (e) {
      print(response.statusCode);
    }
    return listProducts;
  }
}
