import 'dart:convert';

class Category {
  int? id;
  String name;
  String? photo;

  Category({
    this.id,
    required this.name,
    this.photo
  });

  factory Category.fromJson(String str) => Category.fromMap(json.decode(str));

  factory Category.fromMap(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        photo : json['photo']
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "photo":photo
      };
}
