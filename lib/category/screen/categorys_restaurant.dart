// ignore_for_file: sized_box_for_whitespace

import 'package:curso_udemy/category/category_export.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategorysRestaurant extends StatelessWidget {
  final int id;

  const CategorysRestaurant({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final categoryProvider = Provider.of<CategoryProvider>(context);
    return SliverList(
        delegate: SliverChildListDelegate(
      [ListViewCategory(categoryProvider: categoryProvider, id: id)],
    ));
  }
}
