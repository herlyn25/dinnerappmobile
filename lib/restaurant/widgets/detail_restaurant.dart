import 'package:curso_udemy/export_general.dart';
import 'package:flutter/material.dart';

class DetailRestaurant extends StatelessWidget {
  const DetailRestaurant({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Restaurant restaurant =
        ModalRoute.of(context)?.settings.arguments as Restaurant;
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          AppBarResraurant(
            photo: restaurant.photo,
            name: restaurant.name,
          ),
          CategorysRestaurant(
            id: restaurant.id!,
          )
        ],
      ),
    );
  }
}
