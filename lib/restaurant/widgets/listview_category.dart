import 'package:curso_udemy/export_general.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListViewCategory extends StatelessWidget {
  const ListViewCategory({
    Key? key,
    required this.categoryProvider,
    required this.id,
  }) : super(key: key);

  final CategoryProvider categoryProvider;
  final int id;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: categoryProvider.getCategory('product/?restaurant=', id),
        builder: (_, AsyncSnapshot<List<Category>> snapshot) {
          if (!snapshot.hasData) {
            return Container(
              height: 180,
              child: const CupertinoActivityIndicator(),
            );
          }
          final List<Category> category = snapshot.data!;

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Categories",
                textAlign: TextAlign.start,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
              ),
              SizedBox(
                height: 250,
                width: double.infinity,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: category.length,
                    itemBuilder: (_, int index) => Container(
                          margin: const EdgeInsets.only(top: 30),
                          padding: const EdgeInsets.only(right: 20),
                          child: Row(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Column(
                                  children: [
                                    GestureDetector(
                                      onTap: (() {
                                        categoryProvider
                                            .getProductByCategoryAndRestaurant(
                                                id, category[index].id!);
                                      }),
                                      child: CircleAvatar(
                                        backgroundColor: Colors.white,
                                        backgroundImage: NetworkImage(
                                            category[index].photo!),
                                        radius: 45,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      category[index].name,
                                      style: const TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )),
              ),
            ],
          );
        });
  }
}
