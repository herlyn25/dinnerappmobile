// ignore_for_file: sized_box_for_whitespace

import 'package:card_swiper/card_swiper.dart';
import 'package:curso_udemy/restaurant/models/restaurant.dart';
import 'package:flutter/material.dart';

class SwiperSlider extends StatelessWidget {
  final List<Restaurant> restaurant;
  const SwiperSlider({Key? key, required this.restaurant}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: double.infinity,
      height: size.height * 0.45,
      child: Swiper(
        viewportFraction: 0.7,
        scale: 0.8,
        itemCount: restaurant.length,
        itemWidth: 200,
        itemHeight: 100,
        itemBuilder: ((context, index) {
          return GestureDetector(
            onTap: () => Navigator.pushNamed(context, 'detail_restaurant',
                arguments: restaurant[index]),
            child: Column(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: FadeInImage(
                    placeholder: const AssetImage('assets/no-image.jpg'),
                    image: NetworkImage(restaurant[index].photo),
                    fit: BoxFit.cover,
                    height: 350,
                    width: 250,
                  ),
                ),
                const SizedBox(height: 10),
                Text(
                  restaurant[index].name,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20),
                )
              ],
            ),
          );
        }),
      ),
    );
  }
}
