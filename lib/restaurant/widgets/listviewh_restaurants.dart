import 'package:curso_udemy/restaurant/restaurant_export.dart';
import 'package:flutter/material.dart';

class ListViewHRestaurants extends StatelessWidget {
  final List<Restaurant> restaurant;
  const ListViewHRestaurants({Key? key, required this.restaurant})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: SizedBox(
        width: double.infinity,
        height: 260,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                'Populares',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: restaurant.length,
                  itemBuilder: (_, int index) {
                    return GestureDetector(
                      onTap: () => Navigator.pushNamed(
                          context, 'detail_restaurant',
                          arguments: restaurant[index]),
                      child: Container(
                        child: Column(children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: FadeInImage(
                              width: 130,
                              height: 190,
                              placeholder:
                                  const AssetImage('assets/no-image.jpg'),
                              image: NetworkImage(restaurant[index].photo),
                              fit: BoxFit.cover,
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            restaurant[index].name,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            textAlign: TextAlign.center,
                          ),
                        ]),
                        margin: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 10),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
