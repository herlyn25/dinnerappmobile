import 'package:flutter/material.dart';

import '../../theme/app_theme.dart';

class AppBarResraurant extends StatelessWidget {
  final String photo;
  final String name;

  const AppBarResraurant({Key? key, required this.photo, required this.name})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
        backgroundColor: AppTheme.primary,
        expandedHeight: 200,
        floating: false,
        pinned: true,
        flexibleSpace: FlexibleSpaceBar(
          background: FadeInImage(
            image: NetworkImage(photo),
            placeholder: const AssetImage('assets/loading.gif'),
            fit: BoxFit.cover,
          ),
          title: Container(
            width: double.infinity,
            alignment: Alignment.bottomCenter,
            color: Colors.black12,
            child: Text(name),
          ),
        ));
  }
}
