// ignore_for_file: prefer_const_constructors

import 'package:curso_udemy/export_general.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RestaurantHome extends StatelessWidget {
  const RestaurantHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final restaurantProvider = Provider.of<RestaurantProvider>(context);
    return Scaffold(
      appBar: AppBar(title: Text('Restaurantes')),
      body: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        SizedBox(
          height: 10,
        ),
        SwiperSlider(restaurant: restaurantProvider.listRestaurant),
        SizedBox(
          height: 20,
        ),
        ListViewHRestaurants(
          restaurant: restaurantProvider.listRestaurant,
        )
      ]),
    );
  }
}
