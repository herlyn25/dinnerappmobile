// ignore_for_file: non_constant_identifier_names, avoid_function_literals_in_foreach_calls
import 'dart:convert';
import 'package:curso_udemy/restaurant/models/restaurant.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class RestaurantProvider extends ChangeNotifier {
  static const baseUrl = "https://mydinnerapp.herokuapp.com/";
  static const headers = {"Content-Type": "application/json;charset=UTF-8"};
  List<Restaurant> listRestaurant = [];
  RestaurantProvider() {
    getOnDisplayRestaurant('restaurant/');
  }
  getOnDisplayRestaurant(String path) async {
    final url = Uri.parse('$baseUrl$path');
    final response = await http.get(url, headers: headers);
    try {
      List listRestaurantResponse = json.decode(response.body);
      listRestaurantResponse.forEach((element) {
        listRestaurant.add(Restaurant.fromMap(element));
      });
      if (listRestaurant.isEmpty) {
        Restaurant r = Restaurant(
            name: 'No Hay Restaurant',
            photo:
                'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/2048px-No_image_available.svg.png');
        listRestaurant.add(r);
      }
    } catch (e) {
      print(e.toString());
    }
    notifyListeners();
  }
}
