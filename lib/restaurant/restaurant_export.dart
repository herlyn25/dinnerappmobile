export 'package:curso_udemy/restaurant/widgets/detail_restaurant.dart';
export 'package:curso_udemy/restaurant/screens/restaurant_home.dart';
export 'package:curso_udemy/restaurant/widgets/listviewh_restaurants.dart';
export 'package:curso_udemy/restaurant/widgets/swipe_restaurants.dart';
export 'package:curso_udemy/restaurant/widgets/appbar_restaurant.dart';
export 'package:curso_udemy/restaurant/models/restaurant.dart';
export 'package:curso_udemy/restaurant/provider/restaurant_provider.dart';