import 'dart:convert';

class Restaurant {
  Restaurant({
    this.id,
    required this.name,
    required this.photo,
    this.email,
    this.phone,
    this.description,
  });

  int? id;
  String name;
  String photo;
  String? email;
  int? phone;
  String? description;

  factory Restaurant.fromJson(String str) =>
      Restaurant.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Restaurant.fromMap(Map<String, dynamic> json) => Restaurant(
        id: json["id"],
        name: json["name"],
        photo: json["photo"],
        email: json["email"],
        phone: json["phone"],
        description: json["description"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "photo": photo,
        "email": email,
        "phone": phone,
        "description": description,
      };
}
